# Flutter Starter
Flutter application that will be used by the candidates interviewing at [Talpoint][1].

## Dependencies
This project relies on Flutter, mainly:
- Flutter 2.10.5
- Dart 2.16.2

## Requirements
- Candidates are expected to clone this repo and implement required features based on inputs provided by the interviewer
- Be ready with a cloned version of this repo well before the start of interview.

## Nice to have
It will be an advantage for candidates to demonstrate the following:
- Understanding of Flutter, and Dart best practices.

[1]: https://talpoint.in
